# comet-example

## Setup

Install dependencies
```
pip install -r requirements.txt
```
Set Comet Environment Variables
```
export COMET_API_KEY="Your API Key"
export COMET_WORKSPACE="Your Workspace Name"
export COMET_PROJECT_NAME="Your Project Name"
```

## Training
```
python mnist-dnn-rich.py
```
This script will train a simple Neural Network on the MNIST dataset and upload a model as an Experiment Asset. 
By default the model name will be `mnist-neural-net`. The name can be changed by passing in a command line argument 
for the model name.  

## Register a Model

Before you can download a model from Comet, you must register it in the Model Registry. Alternatively, you can register a model through the [Comet UI](https://www.comet.ml/site/using-comet-model-registry/#:~:text=Register%20a%20Model&text=To%20register%20an%20experiment's%20model,The%20Register%20Existing%20Model%20dialog.), and skip this step.  
```
python register_model.py \
--workspace "Your Workspace Name"
--project_name "Your Project Name"
--experiment_id "Experiment ID that created the model"
--model_name mnist-neural-net
--model_version "Semantic Version of the Model. Defaults to 1.0.0"
```

## Downloading a Model from Model Registry
```
python download_model.py \
--model_name mnist-neural-net \
--model_version "Model Version to Download, if not provided defaults to 1.0.0"
--output_path "path to where model should be saved"
```
